import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'DTC475Project1';
  decolCheck = true;
  soverCheck = true;
  ICTCheck = true;
  contextCheck = true;
  importantCheck = true;


  reveal(x:string) {
      if (x == 'hiddenDecolonization') {
        var elem = document.getElementById(x)!;
        var part1 = document.getElementById("part1")!;
        if (this.decolCheck == true) {
          this.decolCheck = false;
          elem.style.opacity = '100%';
          part1.style.opacity = '100%';

        }
        else if (this.decolCheck == false) {
          this.decolCheck = true;
          elem.style.opacity = '0%';

        }
      }
      else if (x == 'hiddenSovereignty') {
        var elem = document.getElementById(x)!;
        var part2 = document.getElementById("part2")!;
        if (this.soverCheck == true) {
          this.soverCheck = false;
          elem.style.opacity = '100%';
          part2.style.opacity = '100%';
        }
        else if (this.soverCheck == false) {
          this.soverCheck = true;
          elem.style.opacity = '0%';

        }
      }
      else if (x == 'hiddenICT') {
        var elem = document.getElementById(x)!;
        var part3 = document.getElementById("part3")!;
        if (this.ICTCheck == true) {
          this.ICTCheck = false;
          elem.style.opacity = '100%';
          part3.style.opacity = '100%';
        }
        else if (this.ICTCheck == false) {
          this.ICTCheck = true;
          elem.style.opacity = '0%';

        }
      }
      else if (x == 'hiddenContext') {
        var elem = document.getElementById(x)!;
        var part4 = document.getElementById("part4")!;
        if (this.contextCheck == true) {
          this.contextCheck = false;
          elem.style.opacity = '100%';
          part4.style.opacity = '100%';
        }
        else if (this.contextCheck == false) {
          this.contextCheck = true;
          elem.style.opacity = '0%';

        }
      }
      else if (x == 'hiddenImportant') {
        var elem = document.getElementById(x)!;
        if (this.importantCheck == true) {
          this.importantCheck = false;
          elem.style.opacity = '100%';
        }
        else if (this.importantCheck == false) {
          this.importantCheck = true;
          elem.style.opacity = '0%';

        }
      }


  }
}


